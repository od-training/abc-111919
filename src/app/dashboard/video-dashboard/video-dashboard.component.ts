import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { HttpClient } from '@angular/common/http';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  selectedVideo: Video | undefined;
  videoList: Observable<Video[]>;

  constructor(vds: VideoDataService) {
    this.videoList = vds.loadVideos();
  }

  ngOnInit() {}

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
  }
}
