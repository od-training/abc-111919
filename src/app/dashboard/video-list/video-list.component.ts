import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  selectedVideo: Video | undefined;
  @Input() videos: Video[];
  @Output() chooseVideo = new EventEmitter<Video>();

  constructor() {}

  ngOnInit() {}

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
    this.chooseVideo.emit(video);
  }
}
